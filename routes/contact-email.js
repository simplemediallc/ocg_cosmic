var contactEmail = (app) => {
  var bodyParser = require('body-parser');
  app.use(bodyParser.json()); // support json encoded bodies
  app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

  app.post('/contact', function (req, res) {
    var nodemailer = require("nodemailer");
    //var transporter = nodemailer.createTransport({direct: true, debug: true});
    var transporter = nodemailer.createTransport('smtps://chris@opmi-tx.com:f@ct0ry33@smtp.gmail.com');

    let messageBody = '<p>Dear Admin</p>' +
      '<p>Client with following details has posted and inquiry on Overly Amenities</p>' +
      '<p>' +
      'Name: ' + req.body.fullname + '<br/>' +
      'Email: ' + req.body.email + '<br/>' +
      'Subject: ' + req.body.subject+ '<br/>' +
      'Message: ' + req.body.message + '<br/>' +
      '</p>';

    var mailOptions = {
      from: "Webmaster <chris@osa-tx.com>", // sender address
      to: "opmi@opmi-tx.com", // list of receivers
      subject: "A new inquiry was posted on Overly Amenities", // Subject line
      html: messageBody // html body
    };

    transporter.sendMail(mailOptions, function(error, info) {
      if(error) {
        console.log(error);
        res.json({success: 0});
      } else {
        console.log('Message sent: ' + info.messageId);
        res.json({success: 1});
      }
    });
  })
};

module.exports = contactEmail;