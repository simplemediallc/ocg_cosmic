import * as cosmicCalls from './cosmic-calls';

var pages = {
    '': [{type: 'globals', list: [false, 'contact-info']}, {type: 'projects', list: [true]}, {type: 'news', list: [true]} ],
    'about': [{type: 'globals', list: [false, 'contact-info']}],
    'services': [{type: 'globals', list: [false, 'contact-info']}],
    'contact': [{type: 'globals', list: [false, 'contact-info']}],
    'privacy': [{type: 'globals', list: [false, 'globals']}],
    'thank-you': [{type: 'globals', list: [false, 'contact-info']}],
};

var routes = (app, partials) => {
    app.get('(?!/news|/projects)(/((\\w+-?){1,3})?)', function (req, res, next) {
        var path = req.url.replace(/(\/)|(\?.*)/g, '')
        if (pages.hasOwnProperty(path)) {
            cosmicCalls.getData(partials, path, pages[path], res)
            /*let page = (path === '') ? 'index.html' : path + '.html'

            return res.render(page, {
                partials
            })*/
        } else {
            return res.status(404).render('404.html', {
                partials
            })
        }
    })
}

module.exports = routes;
