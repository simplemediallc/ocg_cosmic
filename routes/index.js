module.exports = (app, partials) => {
  require('./home')(app, partials)
  var news = require('./news')(app, partials)
  var projects = require('./projects')(app, partials)

  app.use('/news', news)
  app.use('/projects', projects)
}