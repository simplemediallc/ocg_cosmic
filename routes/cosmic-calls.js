import Cosmic from 'cosmicjs'
import truncate from 'html-truncate'
import dateFormat from 'dateformat'

var config = {}

config.bucket = {
  slug: 'overly-construction-group',
  read_key: '', // add read_key if added to your Cosmic JS bucket settings
  write_key: '' // add write_key if added to your Cosmic JS bucket settings
}

function fetchData(partials, path, types, res, offset) {
  let objectType = types[offset]
  if (typeof objectType === "undefined") {
    let page = (path === '') ? 'index.html' : path + '.html'
    
    return res.render(page, {
      partials
    })
  } else {
    if (objectType.list[0])
      fetchObjectType(partials, path, types, res, offset)
    else
      fetchObject(partials, path, types, res, offset)
  }
}

function fetchObjectType(partials, path, types, res, offset) {
  let objectType = types[offset]

  let limit = 0
  if(path == 'news')
    limit = 6
  else if(path == '')
    limit = 3

  var params = {
    type_slug: objectType.type,
    limit: limit,
    skip: 0
  };

  Cosmic.getObjectType(config, params, (err, response) => {
    if (err) console.error('There was an error', err)

    if(response.objects.all) {
      res.locals[objectType.type] = response.objects.all

      if(objectType.type == 'news') {
        res.locals['load-more'] = !(response.total > response.objects.all.length) ? 'style="display:none;"' : ''

        res.locals.page = {title: 'News'}
        res.locals.news.forEach((item, i) => {
          res.locals.news[i].content = truncate(item.content, 150);
          res.locals.news[i].created = {day: dateFormat(item.created, "dd"), month: dateFormat(item.created, "mmm")};
        })
      } else if(objectType.type == 'projects') {
        res.locals.page = {title: 'Projects'}
        res.locals.projects.forEach((item, i) => {
          res.locals.projects[i].content = truncate(item.content, 250);

          var categoryFilter = '';
          res.locals.projects[i].metadata.categories.forEach((item, i) => {
            if(i == 0)
              categoryFilter = item.category.slug
            else
              categoryFilter += ', ' + item.category.slug
          })
          res.locals.projects[i].category_filter = categoryFilter
        })
      }
    }

    fetchData(partials, path, types, res, ++offset)
  })
}

function fetchObject(partials, path, types, res, offset) {
  let objectType = types[offset]

  let params = {
    slug: objectType.list[1],
  };

  Cosmic.getObject(config, params, (err, response) => {
    if (err) console.error('There was an error', err)

  if (response.object) {
    if(objectType.type == 'page')
      res.locals.page = response.object
    else
      res.locals[objectType.list[1]] = response.object
  }

  fetchData(partials, path, types, res, ++offset)
})
}

export function getData(partials, path, types, res) {
  fetchData(partials, path, types, res, 0)
}
