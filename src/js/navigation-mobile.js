(function($) {

	// cache DOM
	var hamburgerButton = $('.hamburger-js');

	// render
	render();

	/**
	 * [render]
	 * @return {[void]}
	 */
	function render() {
		// initialize events
		_initEvents();
	}

	/**
	 * [_initEvents]
	 * @return {[void]}
	 */
	function _initEvents() {
		hamburgerButton.on('click', _onHamburgerClickHandler);
	}

	/**
	 * [_onHamburgerClickHandler]
	 * @return {[void]}
	 */
	function _onHamburgerClickHandler() {
		var _this = $(this);
		var $parent = _this.closest('.navigation-mobile');
		var windowHeight = $(window).outerHeight();

		if ($parent.hasClass('open')) {
			$parent.removeClass('open');
			$parent.css('height', '80px');
			$('body').css('overflow', 'initial');
		} else {
			$parent.addClass('open');
			$parent.css('height', windowHeight + 'px');
			$('body').css('overflow', 'hidden');
		}
	}

})(jQuery);