(function($) {

	// cache DOM
	var $gallery = $('.gallery-js');

	render();

	/**
	 * [render]
	 * @return {[void]}
	 */
	function render() {
		$gallery.lightGallery({
			download: false
		});
	}

})(jQuery);