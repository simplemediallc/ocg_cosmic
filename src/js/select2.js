(function($) {

	// cache DOM
	$projectFilter = $('.projects__filter-js');

	render();

	/**
	 * [render]
	 * @return {[void]}
	 */
	function render() {
		$projectFilter.select2();
	}

})(jQuery);