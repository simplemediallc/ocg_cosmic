(function($) {

	// cache DOM
	$banner = $('.banner__images-js');
	$bannerServicesContent = $('.banner-services__content-js');
	$bannerServicesImages = $('.banner-services__images-js');

	render();

	/**
	 * [render]
	 * @return {[void]}
	 */
	function render() {
		$banner.slick({
			arrows: false,
			dots: true,
			fade: true,
			autoplay: true,
			autoplaySpeed: 6000,
			speed: 1400
		});

		$bannerServicesContent.slick({
			arrows: false,
			dots: true,
			fade: true,
			autoplay: true,
			autoplaySpeed: 6000,
			speed: 1400,
			adaptiveHeight: true,
			asNavFor: $bannerServicesImages
		});

		$bannerServicesImages.slick({
			arrows: false,
			fade: true,
			autoplay: true,
			autoplaySpeed: 6000,
			speed: 1400,
			asNavFor: $bannerServicesContent
		});
	}

})(jQuery);