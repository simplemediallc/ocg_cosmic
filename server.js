import express from 'express'
import hogan from 'hogan-express'

const app = express();

app.engine('html', hogan);

var port = process.env.PORT || 5000;
app.set('port', port);

app.use(express.static('dist'));

const partials = {
  header: 'partials/header',
  banner1: 'partials/banner-01',
  navigation_desktop_light: 'partials/navigation-desktop-light',
  navigation_desktop: 'partials/navigation-desktop',
  navigation_mobile: 'partials/navigation-mobile',
  sub_footer1: 'partials/sub-footer-01',
  sub_footer2: 'partials/sub-footer-02',
  footer: 'partials/footer-01',
  footer1: 'partials/footer-02',
  footer2: 'partials/footer-03'
}

require('./routes')(app, partials);
require('./routes/contact-email')(app);

app.listen(app.get('port'));